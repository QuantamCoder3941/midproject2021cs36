﻿using CRUD_Operations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project
{
    public partial class Evaluate_Student : Form
    {
        List<ComboBox> comboBoxList = new List<ComboBox>();
        List<Label> labelList = new List<Label>();
        string regno;
        public Evaluate_Student(string regno)
        {
            InitializeComponent();
            this.regno = regno;
        }

        private void Evaluate_Student_Load(object sender, EventArgs e)
        {
            LoadAssessmentIntoCB();
        }
        /*private void LoadDataIntoCB()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Title FROM Assessment", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (cbAssessments.Items.Contains(dt.Rows[i][0]) == false)
                {
                    cbAssessments.Items.Add(dt.Rows[i][0]);
                }
            }
        }*/

        private void btn_add_Click(object sender, EventArgs e)
        {
            string assessmentId = getAssessmentId(cbAssessments.Text);
            showAssessmentComponent(assessmentId);
        }


        public string getName()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select CONCAT(FirstName, ' ', lastName) FROM Student WHERE RegistrationNumber=@RegistrationNumber", con);
            cmd.Parameters.AddWithValue("@RegistrationNumber", this.regno);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows[0][0].ToString();
        }

        public void LoadAssessmentIntoCB()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Title FROM Assessment", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (cbAssessments.Items.Contains(dt.Rows[i][0]) == false)
                {
                    cbAssessments.Items.Add(dt.Rows[i][0]);
                }
            }
        }

        private void GenerateComboBox(ComboBox cb)
        {
            // Create a new ComboBox
            ComboBox comboBox = new ComboBox();

            // Set the ComboBox properties
            comboBox.Location = new Point(18, panel2.Controls.Count * 30 + 40);
            comboBox.Size = new Size(204, 37);
            comboBox.Font = new Font("Lato", 12);
            for (int i = 0; i < cb.Items.Count; i++)
            {
                comboBox.Items.Add(cb.Items[i]);
            }

            // Add the ComboBox to the form
            panel2.Controls.Add(comboBox);
            comboBoxList.Add(comboBox);

            comboBox.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
        }

        private void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Get the ComboBox that fired the event
            ComboBox comboBox = (ComboBox)sender;

            // Determine which ComboBox fired the event by its index in the list
            int comboBoxIndex = comboBoxList.IndexOf(comboBox);

            // Get the selected item from the ComboBox
            string selectedValue = comboBox.SelectedItem.ToString();
        }

        private void GenerateLabel(string assessmentComponent, int location)
        {
            // Create a new Label
            Label label = new Label();

            // Set the Label properties
            label.Name = "label" + location;
            label.Location = new Point(18, panel2.Controls.Count * 30 + 10);
            label.Size = new Size(200, 30);
            label.Font = new Font("Lato", 12);
            label.ForeColor = Color.FromArgb(49, 119, 115);
            label.Text = assessmentComponent;
            string rubricId = getRubricId(assessmentComponent);
            showRubricLevels(rubricId);
            labelList.Add(label);
            // Add the Label to the form
            panel2.Controls.Add(label);
        }

        private void cbAssessmentId_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private string getAssessmentId(string assessment)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id FROM Assessment WHERE Title=@Title", con);
            cmd.Parameters.AddWithValue("@Title", assessment);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows[0][0].ToString();
        }

        private string getRubricId(string name)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select RubricId FROM AssessmentComponent WHERE Name=@Name", con);
            cmd.Parameters.AddWithValue("@Name", name);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows[0][0].ToString();
        }

        private void showAssessmentComponent(string assessmentId)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Name FROM AssessmentComponent WHERE AssessmentId=@AssessmentId", con);
            cmd.Parameters.AddWithValue("@AssessmentId", assessmentId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            int location = 160;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                GenerateLabel(dt.Rows[i][0].ToString(), location + 70);
                location = location + 70;
            }
        }

        private void showRubricLevels(string rubricId)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Details FROM RubricLevel WHERE RubricId=@RubricId", con);
            cmd.Parameters.AddWithValue("@RubricId", rubricId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            ComboBox cb = new ComboBox();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                cb.Items.Add(dt.Rows[i][0]);
            }
            GenerateComboBox(cb);
        }

        private bool isExist(string regNo, string assessmentComponentId)
        {
            bool exist = false;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT StudentId FROM StudentResult WHERE StudentId=@StudentId AND AssessmentComponentId=@AssessmentComponentId", con);
            cmd.Parameters.AddWithValue("@StudentId", getId(regNo));
            cmd.Parameters.AddWithValue("@AssessmentComponentId", getAssessmentComponentId(assessmentComponentId));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                return exist;
            }
            else
            {
                exist = true;
            }
            return exist;
        }

        private bool checkCB(List<ComboBox> comboBoxList)
        {
            for (int i = 0; i < comboBoxList.Count; i++)
            {
                if (comboBoxList[i].Text == "")
                {
                    return false;
                }
            }
            return true;
        }

        private string getId(string regNo)
        {
            string Id = "";
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Id FROM Student WHERE RegistrationNumber=@RegistrationNumber", con);
            cmd.Parameters.AddWithValue("@RegistrationNumber", regNo);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                return Id;
            }
            else
            {
                Id = dt.Rows[0][0].ToString();
            }
            return Id;
        }

        private string getAssessmentComponentId(string assessmentComponent)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id FROM AssessmentComponent WHERE Name=@Name", con);
            cmd.Parameters.AddWithValue("@Name", assessmentComponent);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows[0][0].ToString();
        }

        private string getRubricMeasurementId(string details, string rubricId)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id FROM RubricLevel WHERE Details=@Details AND RubricId=@RubricId", con);
            cmd.Parameters.AddWithValue("@Details", details);
            cmd.Parameters.AddWithValue("@RubricId", rubricId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows[0][0].ToString();
        }

        private string getRubricIdByComponent(string id)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select RubricId FROM AssessmentComponent WHERE Id=@Id", con);
            cmd.Parameters.AddWithValue("@Id", id);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows[0][0].ToString();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (checkCB(this.comboBoxList) == false)
                {
                    throw new Exception("Please select all the combo boxes");
                }

                for (int i = 0; i < comboBoxList.Count; i++)
                {
                    if (isExist(this.regno, labelList[i].Text))
                    {
                        throw new Exception("Already Exist.");
                    }
                    string assessmentComponentId = getAssessmentComponentId(labelList[i].Text);
                    MessageBox.Show(assessmentComponentId);
                    string rubricId = getRubricIdByComponent(assessmentComponentId);
                    DateTime date = DateTime.Now;
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("INSERT INTO StudentResult VALUES (@StudentId,@AssessmentComponentId,@RubricMeasurementId,@EvaluationDate)", con);
                    cmd.Parameters.AddWithValue("@StudentId", getId(this.regno));
                    cmd.Parameters.AddWithValue("@AssessmentComponentId", assessmentComponentId);
                    cmd.Parameters.AddWithValue("@RubricMeasurementId", getRubricMeasurementId(comboBoxList[i].Text, rubricId));
                    cmd.Parameters.AddWithValue("@EvaluationDate", date);
                    cmd.ExecuteNonQuery();
                }
                this.Close();
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }
        


        /*private int get_assessId(string name)
        {
            int id = 0;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id FROM Assessment where Title=@Title", con);
            cmd.Parameters.AddWithValue("@Title", name);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return int.Parse(dt.Rows[0][0].ToString());
        }
        private void Load_Components()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Name FROM AssessmentComponent where AssessmentId=@AssessmentId", con);
            cmd.Parameters.AddWithValue("@AssessmentId", get_assessId(cbAssessments.Text));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            foreach (string s in asscomp(get_assessId(cbAssessments.Text)))
            {
                showRubricLevels(getRubricId(s));
            }
        }
        private void GenerateComboBox(ComboBox cb)
        {
            // Create a new ComboBox
            ComboBox comboBox = new ComboBox();

            // Set the ComboBox properties

            comboBox.Location = new Point(18, panel2.Controls.Count * 30 + 40);
            comboBox.Size = new Size(204, 37);
            comboBox.Font = new Font("Lato", 12);
            for (int i = 0; i < cb.Items.Count; i++)
            {
                comboBox.Items.Add(cb.Items[i]);
            }

            // Add the ComboBox to the form
            panel2.Controls.Add(comboBox);
            comboBoxList.Add(comboBox);

        }
        private void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Get the ComboBox that fired the event
            ComboBox comboBox = (ComboBox)sender;

            // Determine which ComboBox fired the event by its index in the list
            int comboBoxIndex = comboBoxList.IndexOf(comboBox);

            // Get the selected item from the ComboBox
            string selectedValue = comboBox.SelectedItem.ToString();
        }
        private void GenerateLabel(string assessmentComponent)
        {
            // Create a new Label
            Label label = new Label();

            // Set the Label properties
            label.Location = new Point(18, panel2.Controls.Count * 30 + 10);
            label.Size = new Size(200, 30);
            label.Font = new Font("Lato", 12);
            label.ForeColor = Color.FromArgb(49, 119, 115);
            label.Text = assessmentComponent;
            string rubricId = getRubricId(assessmentComponent);
            showRubricLevels(rubricId);
            labelList.Add(label);
            // Add the Label to the form
            panel2.Controls.Add(label);
        }
        private string getRubricId(string assesscomp)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select RubricID FROM AssessmentComponent where Name=@Name", con);
            cmd.Parameters.AddWithValue("@Name", assesscomp);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows[0][0].ToString();
        }

        private void showRubricLevels(string rubid)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Details From RubricLevel Where RubricId=@RubricId", con);
            cmd.Parameters.AddWithValue("@RubricId", int.Parse(rubid));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            ComboBox comboBox = new ComboBox();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (comboBox.Items.Contains(dt.Rows[i][0]) == false)
                {
                    comboBox.Items.Add(dt.Rows[i][0]);
                }
            }
            GenerateComboBox(comboBox);
        }

        private List<string> asscomp(int id)
        {
            List<string> list = new List<string>();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Name From AssessmentComponent Where AssessmentId=@AssessmentId ", con);
            cmd.Parameters.AddWithValue("@AssessmentId", id);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                list.Add(dt.Rows[i][0].ToString());
            }
            return list;
        }*/

    }
}
    /*private void frmEvaluation_Load(object sender, EventArgs e)
        {
            lblName.Text = "Name: " + getName();
            LoadAssessmentIntoCB();
        }

        public string getName()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select CONCAT(FirstName, ' ', lastName) FROM Student WHERE RegistrationNumber=@RegistrationNumber", con);
            cmd.Parameters.AddWithValue("@RegistrationNumber", this.regNo);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows[0][0].ToString();
        }

        public void LoadAssessmentIntoCB()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Title FROM Assessment", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (cbAssessmentId.Items.Contains(dt.Rows[i][0]) == false)
                {
                    cbAssessmentId.Items.Add(dt.Rows[i][0]);
                }
            }
        }

        private void GenerateComboBox(ComboBox cb, int location)
        {
            // Create a new ComboBox
            ComboBox comboBox = new ComboBox();

            // Set the ComboBox properties
            comboBox.Name = "label" + location;
            comboBox.Location = new Point(18, panel3.Controls.Count * 30 + 40);
            comboBox.Size = new Size(204, 37);
            comboBox.Font = new Font("Lato", 12);
            for (int i = 0; i < cb.Items.Count; i++)
            {
                comboBox.Items.Add(cb.Items[i]);
            }

            // Add the ComboBox to the form
            panel3.Controls.Add(comboBox);
            comboBoxList.Add(comboBox);

            comboBox.SelectedIndexChanged += new EventHandler(comboBox_SelectedIndexChanged);
        }

        private void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Get the ComboBox that fired the event
            ComboBox comboBox = (ComboBox)sender;

            // Determine which ComboBox fired the event by its index in the list
            int comboBoxIndex = comboBoxList.IndexOf(comboBox);

            // Get the selected item from the ComboBox
            string selectedValue = comboBox.SelectedItem.ToString();
        }

        private void GenerateLabel(string assessmentComponent, int location)
        {
            // Create a new Label
            Label label = new Label();

            // Set the Label properties
            label.Name = "label" + location; 
            label.Location = new Point(18, panel3.Controls.Count * 30 + 10);
            label.Size = new Size(200, 30);
            label.Font = new Font("Lato", 12);
            label.ForeColor = Color.FromArgb(49, 119, 115);
            label.Text = assessmentComponent;
            string rubricId = getRubricId(assessmentComponent);
            showRubricLevels(rubricId);
            labelList.Add(label);
            // Add the Label to the form
            panel3.Controls.Add(label);
        }

        private void cbAssessmentId_SelectedIndexChanged(object sender, EventArgs e)
        {
            string assessmentId = getAssessmentId(cbAssessmentId.Text);
            showAssessmentComponent(assessmentId);
            cbAssessmentId.Enabled = false;
        }

        private string getAssessmentId(string assessment)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id FROM Assessment WHERE Title=@Title", con);
            cmd.Parameters.AddWithValue("@Title", assessment);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows[0][0].ToString();
        }

        private string getRubricId(string name)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select RubricId FROM AssessmentComponent WHERE Name=@Name", con);
            cmd.Parameters.AddWithValue("@Name", name);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows[0][0].ToString();
        }

        private void showAssessmentComponent(string assessmentId)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Name FROM AssessmentComponent WHERE AssessmentId=@AssessmentId", con);
            cmd.Parameters.AddWithValue("@AssessmentId", assessmentId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            int location = 160;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                GenerateLabel(dt.Rows[i][0].ToString(), location + 70);
                location = location + 70;
            }
        }

        private void showRubricLevels(string rubricId)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Details FROM RubricLevel WHERE RubricId=@RubricId", con);
            cmd.Parameters.AddWithValue("@RubricId", rubricId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            ComboBox cb = new ComboBox();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                cb.Items.Add(dt.Rows[i][0]);
            }
            GenerateComboBox(cb, this.lct + 90);
            this.lct = this.lct + 70;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (checkCB(this.comboBoxList) == false)
                {
                    throw new Exception("Please select all the combo boxes");
                }

                for (int i = 0; i < comboBoxList.Count; i++)
                {
                    if (isExist(this.regNo, labelList[i].Text))
                    {
                        throw new Exception("Already Exist.");
                    }
                    string assessmentComponentId = getAssessmentComponentId(labelList[i].Text);
                    MessageBox.Show(assessmentComponentId);
                    string rubricId = getRubricIdByComponent(assessmentComponentId);
                    DateTime date = DateTime.Now;
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("INSERT INTO StudentResult VALUES (@StudentId,@AssessmentComponentId,@RubricMeasurementId,@EvaluationDate)", con);
                    cmd.Parameters.AddWithValue("@StudentId", getId(this.regNo));
                    cmd.Parameters.AddWithValue("@AssessmentComponentId", assessmentComponentId);
                    cmd.Parameters.AddWithValue("@RubricMeasurementId", getRubricMeasurementId(comboBoxList[i].Text, rubricId));
                    cmd.Parameters.AddWithValue("@EvaluationDate", date);
                    cmd.ExecuteNonQuery();
                }
                this.Close();
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }

        private bool isExist(string regNo, string assessmentComponentId)
        {
            bool exist = false;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT StudentId FROM StudentResult WHERE StudentId=@StudentId AND AssessmentComponentId=@AssessmentComponentId", con);
            cmd.Parameters.AddWithValue("@StudentId", getId(regNo));
            cmd.Parameters.AddWithValue("@AssessmentComponentId", getAssessmentComponentId(assessmentComponentId));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                return exist;
            }
            else
            {
                exist = true;
            }
            return exist;
        }

        private bool checkCB(List<ComboBox> comboBoxList)
        {
            for (int i = 0; i < comboBoxList.Count; i++)
            {
                if (comboBoxList[i].Text == "")
                {
                    return false;
                }
            }
            return true;
        }

        private string getId(string regNo)
        {
            string Id = "";
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Id FROM Student WHERE RegistrationNumber=@RegistrationNumber", con);
            cmd.Parameters.AddWithValue("@RegistrationNumber", regNo);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                return Id;
            }
            else
            {
                Id = dt.Rows[0][0].ToString();
            }
            return Id;
        }

        private string getAssessmentComponentId(string assessmentComponent)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id FROM AssessmentComponent WHERE Name=@Name", con);
            cmd.Parameters.AddWithValue("@Name", assessmentComponent);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows[0][0].ToString();
        }

        private string getRubricMeasurementId(string details, string rubricId)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id FROM RubricLevel WHERE Details=@Details AND RubricId=@RubricId", con);
            cmd.Parameters.AddWithValue("@Details", details);
            cmd.Parameters.AddWithValue("@RubricId", rubricId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows[0][0].ToString();
        }

        private string getRubricIdByComponent(string id)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select RubricId FROM AssessmentComponent WHERE Id=@Id", con);
            cmd.Parameters.AddWithValue("@Id", id);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows[0][0].ToString();
        }

}
*/