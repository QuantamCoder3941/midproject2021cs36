﻿using CRUD_Operations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project
{
    public partial class clo_update_form : Form
    {
        string id, clo, datecreated, dateupdated;

        private void textBox1_Enter(object sender, EventArgs e)
        {
            if(textBox1.Text == "clo")
            {
                textBox1.Text = "";
                textBox1.ForeColor = Color.Black;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("UPDATE Clo SET Name=@Name, DateCreated=@DateCreated, DateUpdated=@DateUpdated where Id=@Id", con);
            cmd.Parameters.AddWithValue("@Id", this.id);
            cmd.Parameters.AddWithValue("@Name", textBox1.Text);
            cmd.Parameters.AddWithValue("@DateCreated", this.datecreated);
            cmd.Parameters.AddWithValue("@DateUpdated", DateTime.Now);
            
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved");
            this.Close();
        }

        public clo_update_form(string id ,string clo, string datecreated, string dateupdated)
        {
            InitializeComponent();
            this.id = id;
            this.clo = clo;
            this.datecreated = datecreated;
            this.dateupdated = dateupdated;
        }

        private void clo_update_form_Load(object sender, EventArgs e)
        {
            textBox1.ForeColor = Color.Black;
            textBox1.Text = clo;
        }
    }
}
