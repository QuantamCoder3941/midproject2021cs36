﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project
{
    public partial class Dashboard_form : Form
    {
       

        public Dashboard_form()
        {
            InitializeComponent();
            student_Crud1.Hide();
            clOs1.Hide();
            attendance_student1.Hide();
            assesments1.Hide();
            rubric1.Hide();
            rubric_Level1.Hide();
            student_Result1.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            rubric_Level1.Hide();
            attendance_student1.Hide();
            assesments1.Hide();
            rubric1.Hide();
            clOs1.Hide();
            student_Crud1.Show();
            student_Result1.Hide();
        }

        private void Dashboard_form_Load(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_CLO_Click(object sender, EventArgs e)
        {
            rubric_Level1.Hide();
            attendance_student1.Hide();
            assesments1.Hide();
            rubric1.Hide();
            student_Crud1.Hide();
            clOs1.Show();
            student_Result1.Hide();
        }

        private void btn_attendance_Click(object sender, EventArgs e)
        {
            rubric_Level1.Hide();
            attendance_student1.Show();
            assesments1.Hide();
            rubric1.Hide();
            student_Crud1.Hide();
            clOs1.Hide();
            student_Result1.Hide();
        }

        private void btn_assesments_Click(object sender, EventArgs e)
        {
            rubric_Level1.Hide();
            attendance_student1.Hide();
            assesments1.Show();
            rubric1.Hide();
            student_Crud1.Hide();
            clOs1.Hide();
            student_Result1.Hide();
        }

        private void btn_Rubric_Click(object sender, EventArgs e)
        {
            rubric_Level1.Hide();
            rubric1.Show();
            student_Crud1.Hide();
            clOs1.Hide();
            assesments1.Hide();
            attendance_student1.Hide();
            student_Result1.Hide();
        }

        private void btn_classattendence_Click(object sender, EventArgs e)
        {
            student_Crud1.Hide();
            clOs1.Hide();
            attendance_student1.Hide();
            assesments1.Hide();
            rubric1.Hide();
            student_Result1.Hide();
        }

        private void btn_rubriclevel_Click(object sender, EventArgs e)
        {
            rubric_Level1.Show();
            student_Crud1.Hide();
            clOs1.Hide();
            attendance_student1.Hide();
            assesments1.Hide();
            student_Result1.Hide();
        }

        private void btn_result_Click(object sender, EventArgs e)
        {
            student_Result1.Show();
            student_Crud1.Hide();
            clOs1.Hide();
            attendance_student1.Hide();
            assesments1.Hide();
            rubric1.Hide();
            rubric_Level1.Hide();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }
    }
}
