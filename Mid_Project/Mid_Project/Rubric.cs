﻿using CRUD_Operations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Mid_Project
{
    public partial class Rubric : UserControl
    {
        public Rubric()
        {
            InitializeComponent();
            
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Rubric values (@Id,@Details,@CloId)", con);
            cmd.Parameters.AddWithValue("@Id", find_rubric_id());
            cmd.Parameters.AddWithValue("@Details", Details_textBox.Text.ToString());
            if(find_cloid(cbCloids.GetItemText(cbCloids.SelectedItem)) == 0)
            {
                MessageBox.Show("No CLo found!");
            }
            else
            {
                int id = find_cloid(cbCloids.GetItemText(cbCloids.SelectedItem));
                cmd.Parameters.AddWithValue("@CloId", id);
            }
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved");
            binddata();
        }
        public void binddata()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Rubric", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void Details_textBox_Enter(object sender, EventArgs e)
        {
            if(Details_textBox.Text == "Details")
            {
                Details_textBox.Text = "";
                Details_textBox.ForeColor = Color.Black;
            }
        }

        private void cbCloids_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void LoadDataIntoCB()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Name FROM Clo", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (cbCloids.Items.Contains(dt.Rows[i][0]) == false)
                {
                    cbCloids.Items.Add(dt.Rows[i][0]);
                }
            }
        }
        private int find_cloid(string name)
        {
            int id = 0;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id FROM Clo Where Name = @Name", con);
            MessageBox.Show(name);
            cmd.Parameters.AddWithValue("@Name", name);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if(dt.Rows.Count > 0)
            {
                id = int.Parse(dt.Rows[0][0].ToString());
                return id;
            }
            else
            {
                return id;
            }

        }
        private int find_rubric_id()
        {
            int id = 1;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id FROM Rubric ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            int max = 0;
            if (dt.Rows.Count > 0)
            {
                for(int i = 0; i < dt.Rows.Count; i++)
                {
                    if(int.Parse(dt.Rows[i][0].ToString()) > max)
                    {
                        max = int.Parse(dt.Rows[i][0].ToString());
                    }
                }
                id = max+1;
                return id;
            }
            else
            {
                return id;
            }
        }

        private void Rubric_Load(object sender, EventArgs e)
        {
            LoadDataIntoCB();
            binddata();
        }
    }
}
