﻿using CRUD_Operations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TaskbarClock;

namespace Mid_Project
{
    public partial class Rubric_Level : UserControl
    {
        public Rubric_Level()
        {
            InitializeComponent();
            binddata();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            if(cbRubricIds.Text == "RubricId" || Details_textBox.Text == "Details" || Details_textBox.Text == ""  )
            {
                MessageBox.Show("Select all Credentials");
            }
            else
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into RubricLevel values (@RubricId,@Details,@MeasurementLevel)", con);
               
                cmd.Parameters.AddWithValue("@Details", Details_textBox.Text);
                cmd.Parameters.AddWithValue("@RubricId", find_rubricid(cbRubricIds.Text));
                cmd.Parameters.AddWithValue("@MeasurementLevel", find_measurementlevel());
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
            }
            binddata();
        }
        private int find_rubricid(string name)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Rubric where Details=@Details", con);
            cmd.Parameters.AddWithValue("@Details", name);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return int.Parse(dt.Rows[0][0].ToString());
        }
        private int find_measurementlevel()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select max(MeasurementLevel) from RubricLevel where Details=@Details", con);
            cmd.Parameters.AddWithValue("@Details", Details_textBox.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows[0][0].ToString() == "")
            {
                return 1;
            }
            return int.Parse(dt.Rows[0][0].ToString())+1;
        }

        private void Details_textBox_Enter(object sender, EventArgs e)
        {
            if(Details_textBox.Text == "Details")
            {
                Details_textBox.Text = "";
                Details_textBox.ForeColor = Color.Black;
            }
        }

        private void Details_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            /*char ch = e.KeyChar;
            if (char.IsDigit(ch))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }*/
        }

        private void Details_textBox_Leave(object sender, EventArgs e)
        {
            if (Details_textBox.Text == "")
            {
                Details_textBox.Text = "Details";
                Details_textBox.ForeColor = Color.Silver;
            }
        }
        private void Load_Rubrics()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Details from Rubric", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (cbRubricIds.Items.Contains(dt.Rows[i][0]) == false)
                {
                    cbRubricIds.Items.Add(dt.Rows[i][0]);
                }
            }
        }

        private void Rubric_Level_Load(object sender, EventArgs e)
        {
            Load_Rubrics();
            binddata();
        }
        private void binddata()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from RubricLevel", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
