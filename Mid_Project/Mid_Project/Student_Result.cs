﻿using CRUD_Operations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project
{
    public partial class Student_Result : UserControl
    {
        public Student_Result()
        {
            InitializeComponent();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            if(cbStudents.Text != "")
            {
                Form form = new Evaluate_Student(cbStudents.Text);
                form.ShowDialog();
                binddata();
            }
        }
        private void LoadDataIntoCB()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select RegistrationNumber FROM Student where Status=5", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd); 
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (cbStudents.Items.Contains(dt.Rows[i][0]) == false)
                {
                    cbStudents.Items.Add(dt.Rows[i][0]);
                }
            }
        }

        private void Student_Result_Load(object sender, EventArgs e)
        {
            binddata();
            LoadDataIntoCB();
        }
        public void binddata()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from StudentResult", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
