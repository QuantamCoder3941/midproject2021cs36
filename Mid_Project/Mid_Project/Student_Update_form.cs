﻿using CRUD_Operations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Mid_Project
{
    
    public partial class Student_Update_form : Form
    {
        string id, fName, lName, contact, email, regNo, status;
       // id = 1;
        public Student_Update_form(string id,string fname,string lname,string contact,string email,string regno,string status)
        {
            InitializeComponent();
            this.id = id;
            this.fName = fname;
            this.lName = lname;
            this.contact = contact;
            this.email = email;
            this.regNo = regno;
            this.status = status;
        }

        private void txtFirstName_Enter(object sender, EventArgs e)
        {
            if(txtFirstName.Text == "First Name")
            {
                txtFirstName.Text = "";
                txtFirstName.ForeColor = Color.Black;
            }
        } 

        private void txtFirstName_Leave(object sender, EventArgs e)
        {
            if (txtFirstName.Text == "")
            {
                txtFirstName.Text = "First Name";
                txtFirstName.ForeColor = Color.Silver;
            }
        }

        private void txtLastName_Enter(object sender, EventArgs e)
        {
            if(txtFirstName.Text == "Last Name")
            {
                txtFirstName.Text = "";
                txtFirstName.ForeColor = Color.Black;
            }
        }

        private void txtLastName_Leave(object sender, EventArgs e)
        {
            if (txtFirstName.Text == "")
            {
                txtFirstName.Text = "Last Name";
                txtFirstName.ForeColor = Color.Silver;
            }
        }

        private void txtContact_Enter(object sender, EventArgs e)
        {
            if (txtContact.Text == "Contact")
            {
                txtContact.Text = "";
                txtContact.ForeColor = Color.Black;
            }
        }

        private void txtContact_Leave(object sender, EventArgs e)
        {
            if (txtContact.Text == "")
            {
                txtContact.Text = "Contact";
                txtContact.ForeColor = Color.Silver;
            }
        }

        private void txtEmail_Enter(object sender, EventArgs e)
        {
            if (txtEmail.Text == "Email")
            {
                txtEmail.Text = "";
                txtEmail.ForeColor = Color.Black;
            }
        }

        private void txtEmail_Leave(object sender, EventArgs e)
        {
            if (txtEmail.Text == "")
            {
                txtEmail.Text = "Email";
                txtEmail.ForeColor = Color.Silver;
            }
        }

        private void txtRegNo_Enter(object sender, EventArgs e)
        {
            if (txtRegNo.Text == "Registration Number")
            {
                txtRegNo.Text = "";
                txtRegNo.ForeColor = Color.Black;
            }
        }

        private void Student_Update_form_Load(object sender, EventArgs e)
        {
            txtFirstName.Text = this.fName;
            txtFirstName.ForeColor = Color.Black;
            txtLastName.Text = this.lName;
            txtLastName.ForeColor = Color.Black;
            txtContact.Text = this.contact;
            txtContact.ForeColor = Color.Black;
            txtEmail.Text = this.email;
            txtEmail.ForeColor = Color.Black;
            txtRegNo.Text = this.regNo;
            txtRegNo.ForeColor = Color.Black;
            if (this.status == "Active")
            {
                cbStatus.SelectedIndex = 0;
            }
            else
            {
                cbStatus.SelectedIndex = 1;
            }
        }

        private void txtRegNo_Leave(object sender, EventArgs e)
        {
            if (txtRegNo.Text == "")
            {
                txtRegNo.Text = "Registration Number";
                txtRegNo.ForeColor = Color.Silver;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("UPDATE Student SET FirstName=@FirstName, LastName=@LastName, Contact=@Contact, Email=@Email, RegistrationNumber=@RegistrationNumber, Status=@Status WHERE Id=@Id", con);
            cmd.Parameters.AddWithValue("@Id", this.id);
            cmd.Parameters.AddWithValue("@FirstName", txtFirstName.Text);
            cmd.Parameters.AddWithValue("@LastName", txtLastName.Text);
            cmd.Parameters.AddWithValue("@Contact", txtContact.Text);
            cmd.Parameters.AddWithValue("@Email", txtEmail.Text);
            cmd.Parameters.AddWithValue("@RegistrationNumber", txtRegNo.Text);
            if (cbStatus.SelectedIndex == 0)
            {
                cmd.Parameters.AddWithValue("@Status", 5);
            }
            else
            {
                cmd.Parameters.AddWithValue("@Status", 6);
            }
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved");
            clearFields();
            this.Close();
        }

        private void txtFirstName_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (char.IsDigit(ch))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void txtLastName_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (char.IsDigit(ch))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void txtContact_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (char.IsLetter(ch))
            {
                e.Handled = true;
            }
            if (ch == 32)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void btnClearFields_Click(object sender, EventArgs e)
        {
            clearFields();
        }

        private void clearFields()
        {
            txtFirstName.Text = "FirstName";
            txtFirstName.ForeColor = Color.Silver;
            txtLastName.Text = "LastName";
            txtLastName.ForeColor = Color.Silver;
            txtContact.Text = "Contact";
            txtContact.ForeColor = Color.Silver;
            txtEmail.Text = "Email";
            txtEmail.ForeColor = Color.Silver;
            txtRegNo.Text = "Registration Number";
            txtRegNo.ForeColor = Color.Silver;
            cbStatus.SelectedIndex = 0;
        }

        private string getStatus()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Name from Lookup Where LookupId=@Id", con);
            cmd.Parameters.AddWithValue("@Id", this.id);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows[0][0].ToString();
        }
    }
}