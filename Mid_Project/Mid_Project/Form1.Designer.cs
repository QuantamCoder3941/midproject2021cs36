﻿namespace Mid_Project
{
    partial class Dashboard_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.btn_result = new System.Windows.Forms.Button();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btn_rubriclevel = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btn_Rubric = new System.Windows.Forms.Button();
            this.btn_assesments = new System.Windows.Forms.Button();
            this.btn_attendance = new System.Windows.Forms.Button();
            this.btn_CLO = new System.Windows.Forms.Button();
            this.btn_Student = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.student_Result1 = new Mid_Project.Student_Result();
            this.rubric_Level1 = new Mid_Project.Rubric_Level();
            this.rubric1 = new Mid_Project.Rubric();
            this.attendance_student1 = new Mid_Project.Attendance_student();
            this.assesments1 = new Mid_Project.Assesments();
            this.clOs1 = new Mid_Project.CLOs();
            this.student_Crud1 = new Mid_Project.Student_Crud();
            this.button1 = new System.Windows.Forms.Button();
            this.panel13 = new System.Windows.Forms.Panel();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(76)))));
            this.panel1.Controls.Add(this.panel13);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.panel12);
            this.panel1.Controls.Add(this.btn_result);
            this.panel1.Controls.Add(this.panel11);
            this.panel1.Controls.Add(this.panel10);
            this.panel1.Controls.Add(this.btn_rubriclevel);
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.btn_Rubric);
            this.panel1.Controls.Add(this.btn_assesments);
            this.panel1.Controls.Add(this.btn_attendance);
            this.panel1.Controls.Add(this.btn_CLO);
            this.panel1.Controls.Add(this.btn_Student);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(220, 692);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.pictureBox9);
            this.panel12.Location = new System.Drawing.Point(15, 461);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(45, 37);
            this.panel12.TabIndex = 15;
            // 
            // btn_result
            // 
            this.btn_result.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(76)))));
            this.btn_result.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_result.FlatAppearance.BorderSize = 0;
            this.btn_result.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_result.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_result.ForeColor = System.Drawing.Color.Gainsboro;
            this.btn_result.Location = new System.Drawing.Point(0, 455);
            this.btn_result.Name = "btn_result";
            this.btn_result.Size = new System.Drawing.Size(220, 51);
            this.btn_result.TabIndex = 14;
            this.btn_result.Text = "             STUDENT RESULT";
            this.btn_result.UseVisualStyleBackColor = false;
            this.btn_result.Click += new System.EventHandler(this.btn_result_Click);
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.pictureBox7);
            this.panel11.Location = new System.Drawing.Point(15, 461);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(45, 40);
            this.panel11.TabIndex = 13;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.pictureBox8);
            this.panel10.Location = new System.Drawing.Point(15, 411);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(45, 38);
            this.panel10.TabIndex = 11;
            // 
            // btn_rubriclevel
            // 
            this.btn_rubriclevel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(76)))));
            this.btn_rubriclevel.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_rubriclevel.FlatAppearance.BorderSize = 0;
            this.btn_rubriclevel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_rubriclevel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_rubriclevel.ForeColor = System.Drawing.Color.Gainsboro;
            this.btn_rubriclevel.Location = new System.Drawing.Point(0, 404);
            this.btn_rubriclevel.Name = "btn_rubriclevel";
            this.btn_rubriclevel.Size = new System.Drawing.Size(220, 51);
            this.btn_rubriclevel.TabIndex = 10;
            this.btn_rubriclevel.Text = "        RUBRIC LEVEL";
            this.btn_rubriclevel.UseVisualStyleBackColor = false;
            this.btn_rubriclevel.Click += new System.EventHandler(this.btn_rubriclevel_Click);
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.pictureBox6);
            this.panel9.Location = new System.Drawing.Point(15, 359);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(44, 41);
            this.panel9.TabIndex = 9;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.pictureBox5);
            this.panel8.Location = new System.Drawing.Point(15, 308);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(44, 41);
            this.panel8.TabIndex = 8;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.pictureBox4);
            this.panel7.Location = new System.Drawing.Point(15, 257);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(44, 41);
            this.panel7.TabIndex = 8;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.pictureBox3);
            this.panel6.Location = new System.Drawing.Point(15, 206);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(44, 41);
            this.panel6.TabIndex = 7;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.pictureBox2);
            this.panel5.Location = new System.Drawing.Point(13, 155);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(44, 42);
            this.panel5.TabIndex = 6;
            // 
            // btn_Rubric
            // 
            this.btn_Rubric.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(76)))));
            this.btn_Rubric.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_Rubric.FlatAppearance.BorderSize = 0;
            this.btn_Rubric.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Rubric.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Rubric.ForeColor = System.Drawing.Color.Gainsboro;
            this.btn_Rubric.Location = new System.Drawing.Point(0, 353);
            this.btn_Rubric.Name = "btn_Rubric";
            this.btn_Rubric.Size = new System.Drawing.Size(220, 51);
            this.btn_Rubric.TabIndex = 5;
            this.btn_Rubric.Text = "RUBRIC";
            this.btn_Rubric.UseVisualStyleBackColor = false;
            this.btn_Rubric.Click += new System.EventHandler(this.btn_Rubric_Click);
            // 
            // btn_assesments
            // 
            this.btn_assesments.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(76)))));
            this.btn_assesments.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_assesments.FlatAppearance.BorderSize = 0;
            this.btn_assesments.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_assesments.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_assesments.ForeColor = System.Drawing.Color.Gainsboro;
            this.btn_assesments.Location = new System.Drawing.Point(0, 302);
            this.btn_assesments.Name = "btn_assesments";
            this.btn_assesments.Size = new System.Drawing.Size(220, 51);
            this.btn_assesments.TabIndex = 4;
            this.btn_assesments.Text = "     ASSESMENTs";
            this.btn_assesments.UseVisualStyleBackColor = false;
            this.btn_assesments.Click += new System.EventHandler(this.btn_assesments_Click);
            // 
            // btn_attendance
            // 
            this.btn_attendance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(76)))));
            this.btn_attendance.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_attendance.FlatAppearance.BorderSize = 0;
            this.btn_attendance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_attendance.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_attendance.ForeColor = System.Drawing.Color.Gainsboro;
            this.btn_attendance.Location = new System.Drawing.Point(0, 251);
            this.btn_attendance.Name = "btn_attendance";
            this.btn_attendance.Size = new System.Drawing.Size(220, 51);
            this.btn_attendance.TabIndex = 3;
            this.btn_attendance.Text = "       ATTENDANCE";
            this.btn_attendance.UseVisualStyleBackColor = false;
            this.btn_attendance.Click += new System.EventHandler(this.btn_attendance_Click);
            // 
            // btn_CLO
            // 
            this.btn_CLO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(76)))));
            this.btn_CLO.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_CLO.FlatAppearance.BorderSize = 0;
            this.btn_CLO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_CLO.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_CLO.ForeColor = System.Drawing.Color.Gainsboro;
            this.btn_CLO.Location = new System.Drawing.Point(0, 200);
            this.btn_CLO.Name = "btn_CLO";
            this.btn_CLO.Size = new System.Drawing.Size(220, 51);
            this.btn_CLO.TabIndex = 2;
            this.btn_CLO.Text = "CLOs";
            this.btn_CLO.UseVisualStyleBackColor = false;
            this.btn_CLO.Click += new System.EventHandler(this.btn_CLO_Click);
            // 
            // btn_Student
            // 
            this.btn_Student.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(76)))));
            this.btn_Student.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_Student.FlatAppearance.BorderSize = 0;
            this.btn_Student.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Student.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Student.ForeColor = System.Drawing.Color.Gainsboro;
            this.btn_Student.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.btn_Student.Location = new System.Drawing.Point(0, 149);
            this.btn_Student.Name = "btn_Student";
            this.btn_Student.Size = new System.Drawing.Size(220, 51);
            this.btn_Student.TabIndex = 1;
            this.btn_Student.Text = "STUDENTs";
            this.btn_Student.UseVisualStyleBackColor = false;
            this.btn_Student.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(39)))), ((int)(((byte)(58)))));
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(220, 149);
            this.panel2.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(233, 0);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(10);
            this.label1.Size = new System.Drawing.Size(334, 44);
            this.label1.TabIndex = 0;
            this.label1.Text = "Student Management System";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.student_Result1);
            this.panel4.Controls.Add(this.rubric_Level1);
            this.panel4.Controls.Add(this.rubric1);
            this.panel4.Controls.Add(this.attendance_student1);
            this.panel4.Controls.Add(this.assesments1);
            this.panel4.Controls.Add(this.clOs1);
            this.panel4.Controls.Add(this.student_Crud1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(220, 65);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(781, 627);
            this.panel4.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(76)))));
            this.panel3.Controls.Add(this.tableLayoutPanel1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.ForeColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(220, 0);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(5);
            this.panel3.Size = new System.Drawing.Size(781, 65);
            this.panel3.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(230, 0, 0, 0);
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(771, 55);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox9.Image = global::Mid_Project.Properties.Resources.icons8_a_plus_grading_in_report_card_for_brilliant_student_48;
            this.pictureBox9.Location = new System.Drawing.Point(0, 0);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(45, 37);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 1;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox7.Image = global::Mid_Project.Properties.Resources.icons8_assessment_64;
            this.pictureBox7.Location = new System.Drawing.Point(0, 0);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(45, 40);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 0;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox8.Image = global::Mid_Project.Properties.Resources.icons8_report_card_48;
            this.pictureBox8.Location = new System.Drawing.Point(0, 0);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(45, 38);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 0;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox6.Image = global::Mid_Project.Properties.Resources.icons8_evaluation_66;
            this.pictureBox6.Location = new System.Drawing.Point(0, 0);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(44, 41);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 0;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Mid_Project.Properties.Resources.icons8_attendance_80;
            this.pictureBox5.Location = new System.Drawing.Point(-3, 4);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(44, 41);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 0;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox4.Image = global::Mid_Project.Properties.Resources.icons8_assessment_64;
            this.pictureBox4.Location = new System.Drawing.Point(0, 0);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(44, 41);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox3.Image = global::Mid_Project.Properties.Resources.CLO;
            this.pictureBox3.Location = new System.Drawing.Point(0, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(44, 41);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Image = global::Mid_Project.Properties.Resources.graduation_cap;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(44, 42);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Mid_Project.Properties.Resources.education;
            this.pictureBox1.Location = new System.Drawing.Point(40, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(120, 120);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // student_Result1
            // 
            this.student_Result1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.student_Result1.Location = new System.Drawing.Point(0, 0);
            this.student_Result1.Name = "student_Result1";
            this.student_Result1.Size = new System.Drawing.Size(781, 627);
            this.student_Result1.TabIndex = 6;
            // 
            // rubric_Level1
            // 
            this.rubric_Level1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rubric_Level1.Location = new System.Drawing.Point(0, 0);
            this.rubric_Level1.Name = "rubric_Level1";
            this.rubric_Level1.Size = new System.Drawing.Size(781, 627);
            this.rubric_Level1.TabIndex = 5;
            // 
            // rubric1
            // 
            this.rubric1.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.rubric1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rubric1.Location = new System.Drawing.Point(0, 0);
            this.rubric1.Name = "rubric1";
            this.rubric1.Size = new System.Drawing.Size(781, 627);
            this.rubric1.TabIndex = 4;
            // 
            // attendance_student1
            // 
            this.attendance_student1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.attendance_student1.Location = new System.Drawing.Point(0, 0);
            this.attendance_student1.Name = "attendance_student1";
            this.attendance_student1.Size = new System.Drawing.Size(781, 627);
            this.attendance_student1.TabIndex = 3;
            // 
            // assesments1
            // 
            this.assesments1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.assesments1.Location = new System.Drawing.Point(0, 0);
            this.assesments1.Name = "assesments1";
            this.assesments1.Size = new System.Drawing.Size(781, 627);
            this.assesments1.TabIndex = 2;
            // 
            // clOs1
            // 
            this.clOs1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clOs1.Location = new System.Drawing.Point(0, 0);
            this.clOs1.Name = "clOs1";
            this.clOs1.Size = new System.Drawing.Size(781, 627);
            this.clOs1.TabIndex = 1;
            // 
            // student_Crud1
            // 
            this.student_Crud1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.student_Crud1.Location = new System.Drawing.Point(0, 0);
            this.student_Crud1.Name = "student_Crud1";
            this.student_Crud1.Size = new System.Drawing.Size(781, 627);
            this.student_Crud1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(76)))));
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Gainsboro;
            this.button1.Location = new System.Drawing.Point(0, 506);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(220, 54);
            this.button1.TabIndex = 16;
            this.button1.Text = "    ASSESMENT COMPONENTs";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.pictureBox10);
            this.panel13.Location = new System.Drawing.Point(3, 512);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(44, 38);
            this.panel13.TabIndex = 17;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox10.Image = global::Mid_Project.Properties.Resources.icons8_attendance_80;
            this.pictureBox10.Location = new System.Drawing.Point(0, 0);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(44, 38);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 1;
            this.pictureBox10.TabStop = false;
            // 
            // Dashboard_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1001, 692);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.Color.White;
            this.Name = "Dashboard_form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dashboard_Form";
            this.Load += new System.EventHandler(this.Dashboard_form_Load);
            this.panel1.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_Student;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_assesments;
        private System.Windows.Forms.Button btn_attendance;
        private System.Windows.Forms.Button btn_CLO;
        private System.Windows.Forms.Button btn_Rubric;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Student_Crud student_Crud1;
        private System.Windows.Forms.Button btn_rubriclevel;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Button btn_result;
        private CLOs clOs1;
        private Rubric rubric1;
        private Attendance_student attendance_student1;
        private Assesments assesments1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox9;
        private Rubric_Level rubric_Level1;
        private Student_Result student_Result1;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Button button1;
    }
}

