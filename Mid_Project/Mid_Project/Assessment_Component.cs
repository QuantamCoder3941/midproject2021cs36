﻿using CRUD_Operations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project
{
    public partial class Assessment_Component : Form
    {
        int id;
        public Assessment_Component(int id)
        {
            InitializeComponent();
            this.id = id;
        }

        private void Assessment_Component_Load(object sender, EventArgs e)
        {
            Load_Rubrics();
            calculate_marks();
            binddata();
        }
        private int find_rubricid(string name)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Rubric where Details=@Details", con);
            cmd.Parameters.AddWithValue("@Details", name);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return int.Parse(dt.Rows[0][0].ToString());
        }
        private void Load_Rubrics()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Details from Rubric", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (cbRubricIds.Items.Contains(dt.Rows[i][0]) == false)
                {
                    cbRubricIds.Items.Add(dt.Rows[i][0]);
                }
            }
        }

        private void btn_add_Click(object sender, EventArgs e)
        {

            if (cbRubricIds.Text == "RubricId" || title_textBox.Text == "Name" || title_textBox.Text == "" || numericUpDown1.Value < 5)
            {
                MessageBox.Show("Select all Credentials");
            }
            else
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into AssessmentComponent values (@Name,@RubricId,@TotalMarks,@DateCreated,@DateUpdated,@AssessmentId)", con);

                cmd.Parameters.AddWithValue("@Name", title_textBox.Text);
                cmd.Parameters.AddWithValue("@RubricId", find_rubricid(cbRubricIds.Text));
                cmd.Parameters.AddWithValue("@TotalMarks",numericUpDown1.Value);
                cmd.Parameters.AddWithValue("@DateCreated", DateTime.Now);
                cmd.Parameters.AddWithValue("@DateUpdated", DateTime.Now);
                cmd.Parameters.AddWithValue("@AssessmentId", id);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
                binddata();
                calculate_marks();
            }
        }

        private void title_textBox_Enter(object sender, EventArgs e)
        {
            if(title_textBox.Text == "Name")
            {
                title_textBox.Text = "";
                title_textBox.ForeColor = Color.Black;
            }
        }

        private void title_textBox_Leave(object sender, EventArgs e)
        {
            if (title_textBox.Text == "")
            {
                title_textBox.Text = "Name";
                title_textBox.ForeColor = Color.Silver;
            }
        }
        private void binddata()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from AssessmentComponent where AssessmentId=@AssessmentId", con);
            cmd.Parameters.AddWithValue("@AssessmentId", id);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
        private void calculate_marks()
        {
            int marks = 0;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select TotalMarks from AssessmentComponent Where AssessmentId=@AssessmentId", con);
            cmd.Parameters.AddWithValue("@AssessmentId", id);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for(int i = 0; i < dt.Rows.Count; i++)
            {
                marks += int.Parse(dt.Rows[i][0].ToString());
            }
            MessageBox.Show(marks.ToString());
            if(dt.Rows.Count > 0)
            {
                var con1 = Configuration.getInstance().getConnection();
                SqlCommand cmd1 = new SqlCommand("Update Assessment set TotalMarks =@TotalMarks Where Id=@Id", con);
                cmd1.Parameters.AddWithValue("@TotalMarks", marks);
                cmd1.Parameters.AddWithValue("@Id", id);
                cmd1.ExecuteNonQuery();
            }
        }
    }
}
