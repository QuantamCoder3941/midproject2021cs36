﻿using CRUD_Operations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project
{
    public partial class Student_Attendance : Form
    {
        int id;
        DateTime t;
        public Student_Attendance(int id,DateTime time)
        {
            InitializeComponent();
            this.id = id;
            this.t = time;
        }

        private void Student_Attendance_Load(object sender, EventArgs e)
        {
            binddata();
        }

        public void binddata()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student Where Status = 5", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            populateAttendanceIntoCB();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            try
            {
                foreach(DataGridViewRow row in dataGridView1.Rows)
                {
                    DataGridViewComboBoxCell status = row.Cells["Status"] as DataGridViewComboBoxCell;
                    DataGridViewTextBoxCell name = row.Cells["FirstName"] as DataGridViewTextBoxCell;
                    DataGridViewTextBoxCell regNo = row.Cells["RegistrationNumber"] as DataGridViewTextBoxCell;
                    if (status.Value == null)
                    {
                        throw new Exception("Please select all the attendance combo box");
                    }
                    string Name = name.Value.ToString();
                    string Status = "";
                    Status = status.Value.ToString();
                    string RegNo = regNo.Value.ToString();

                    string s = getLookupId(Status);
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("INSERT INTO StudentAttendance VALUES (@AttendanceId,@StudentId,@AttendanceStatus)", con);
                    cmd.Parameters.AddWithValue("@AttendanceId", this.id);
                    cmd.Parameters.AddWithValue("@StudentId", getId(RegNo));
                    cmd.Parameters.AddWithValue("@AttendanceStatus", s);
                    cmd.ExecuteNonQuery();
                    this.Close();
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }
        private string getLookupId(string status)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT LookupId FROM Lookup WHERE Name=@Name", con);
            cmd.Parameters.AddWithValue("@Name", status);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows[0][0].ToString();
        }

        private void addClassAttendance(DateTime date)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("INSERT INTO ClassAttendance VALUES (@AttendanceDate)", con);
            cmd.Parameters.AddWithValue("@AttendanceDate", date.ToString());
            cmd.ExecuteNonQuery();
        }
        private int getId(string regNo)
        {
            int Id = 0;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Id FROM Student WHERE RegistrationNumber=@RegistrationNumber", con);
            cmd.Parameters.AddWithValue("@RegistrationNumber", regNo);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                return Id;
            }
            else
            {
                Id = int.Parse(dt.Rows[0][0].ToString());
            }
            return Id;
        }

        private void populateAttendanceIntoCB()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Name FROM Lookup WHERE Category='ATTENDANCE_STATUS'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            ComboBox cb = new ComboBox();

            for (int j = 0; j < dt.Rows.Count; j++)
            {
                cb.Items.Add(dt.Rows[j][0]);
            }

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)row.Cells["Status"];
                cell.DataSource = cb.Items;
            }

            int i = 0;
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                DataGridViewComboBoxCell cell = (DataGridViewComboBoxCell)row.Cells["Status"];
                dataGridView1.Rows[i].Cells[0].Value = cell.Items[0];
                i++;
            }
        }
       
    }
}
