﻿using CRUD_Operations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project
{
    public partial class Attendance_student : UserControl
    {
        public Attendance_student()
        {
            InitializeComponent();
           /* getStudentIds();
            getAttedanceIds();*/
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textStudentid_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (char.IsLetter(ch))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            if(CheckAttendance())
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into ClassAttendance values (@AttendanceDate)", con);
                cmd.Parameters.AddWithValue("@AttendanceDate", dateTimePicker1.Value);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
                Form f = new Student_Attendance(Search_id(), dateTimePicker1.Value);
                f.ShowDialog();
            }
            else
            {
                MessageBox.Show("Attendance alredy exists");
            }
            
        }
        public void binddata()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from StudentAttendance", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }


        /*private void getStudentIds()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (cbStudentId.Items.Contains(dt.Rows[i][0]) == false)
                {
                    cbStudentId.Items.Add(dt.Rows[i][0]);
                }
            }
        }*/

        /*private void getAttedanceIds()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from ClassAttendance", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (cbAttendance.Items.Contains(dt.Rows[i][0]) == false)
                {
                    cbAttendance.Items.Add(dt.Rows[i][0]);
                }
            }
        }*/

        private int Search_id()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from ClassAttendance where AttendanceDate = @AttendanceDate", con);
            cmd.Parameters.AddWithValue("@AttendanceDate", dateTimePicker1.Value);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if(dt.Rows.Count > 0)
            {
                return int.Parse(dt.Rows[0][0].ToString());
            }
            else
            {
                MessageBox.Show("id does not exist");
                return -1;
            }
        }

        private void Attendance_student_Load(object sender, EventArgs e)
        {
            binddata();
        }

        private bool CheckAttendance()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from ClassAttendance where AttendanceDate = @AttendanceDate", con);
            cmd.Parameters.AddWithValue("@AttendanceDate", dateTimePicker1.Value);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
