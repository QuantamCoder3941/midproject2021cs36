﻿using CRUD_Operations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project
{
    public partial class Assesments : UserControl
    {
        public Assesments()
        {
            InitializeComponent();
            
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            if(title_textBox.Text != "" && numericUpDown1.Value > 5 && title_textBox.Text != "Title" && isExists())
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Assessment values (@Title,@DateCreated,@TotalMarks,@TotalWeightage)", con);
                cmd.Parameters.AddWithValue("@Title", title_textBox.Text.ToString());
                DateTime time = DateTime.Now;
                cmd.Parameters.AddWithValue("@DateCreated", time);
                cmd.Parameters.AddWithValue("@TotalMarks", 0);
                cmd.Parameters.AddWithValue("@TotalWeightage", numericUpDown1.Value);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
                binddata();
            }
            else
            {
                MessageBox.Show("Crieteria not fulfilled");
            }
        }

        private void title_textBox_Enter(object sender, EventArgs e)
        {
            if(title_textBox.Text == "Title")
            {
                title_textBox.Text = "";
                title_textBox.ForeColor = Color.Black;
            }
        }

        private void Totalmarks_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (char.IsLetter(ch))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void TotalWeight_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (char.IsLetter(ch))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
        public void binddata()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Assessment", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void Assesments_Load(object sender, EventArgs e)
        {
            binddata();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns["Add"].Index == e.ColumnIndex)
            {
                int id = int.Parse(dataGridView1.CurrentCell.OwningRow.Cells[2].Value.ToString());
                Form f = new Assessment_Component(id);
                f.ShowDialog();
            }
        }
        private bool isExists()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Assessment where Title=@Title", con);
            cmd.Parameters.AddWithValue("@Title", title_textBox.Text.ToString());
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            if(dt.Rows.Count > 0)
            {
                return false;
            }
            return true;
        }
       
    }
}
