﻿using CRUD_Operations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mid_Project
{
    public partial class CLOs : UserControl
    {
        public CLOs()
        {
            InitializeComponent();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Clo values (@Name,@DateCreated,@DateUpdated)", con);
            cmd.Parameters.AddWithValue("@Name", Name_txtbox.Text.ToString());
            DateTime time = DateTime.Now;
            cmd.Parameters.AddWithValue("@DateCreated", time);
            cmd.Parameters.AddWithValue("@DateUpdated", time);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved");
            binddata();
        }

        private void create_dateTimePicker_ValueChanged(object sender, EventArgs e)
        {

        }

        public void binddata()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Clo", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void Name_txtbox_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void Name_txtbox_Enter(object sender, EventArgs e)
        {
            if (Name_txtbox.Text == "Name")
            {
                Name_txtbox.Text = "";
                Name_txtbox.ForeColor = Color.Black;
            }
        }

        private void CLOs_Load(object sender, EventArgs e)
        {
            binddata();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns["Update"].Index == e.ColumnIndex)
            {
                string id, clo, datecreated, dateupdated;
                id = dataGridView1.CurrentCell.OwningRow.Cells[1].Value.ToString();
                clo = dataGridView1.CurrentCell.OwningRow.Cells[2].Value.ToString();
                datecreated = dataGridView1.CurrentCell.OwningRow.Cells[3].Value.ToString();
                dateupdated = dataGridView1.CurrentCell.OwningRow.Cells[4].Value.ToString();
                Form f = new clo_update_form(id,clo, datecreated, dateupdated);
                f.ShowDialog();
                binddata();
            }
        }
    }
}
