﻿using CRUD_Operations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Mid_Project
{
    public partial class Student_Crud : UserControl
    {
        public Student_Crud()
        {
            InitializeComponent();
            binddata();
        }

        private void Student_Crud_Load(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            if (textBox1.Text == "FirstName")
            {
                textBox1.Text = "";
                textBox1.ForeColor = Color.Black;
            }
        }



        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void LastName_textBox_Enter(object sender, EventArgs e)
        {
            if (LastName_textBox.Text == "LastName")
            {
                LastName_textBox.Text = "";
                LastName_textBox.ForeColor = Color.Black;
            }
        }

        private void Email_textBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void Email_textBox_Enter(object sender, EventArgs e)
        {
            if (Email_textBox.Text == "Email")
            {
                Email_textBox.Text = "";
                Email_textBox.ForeColor = Color.Black;
            }
        }

        private void Contact_textBox_Enter(object sender, EventArgs e)
        {
            if (Contact_textBox.Text == "Contact")
            {
                Contact_textBox.Text = "";
                Contact_textBox.ForeColor = Color.Black;
            }
        }

        private void Registration_textBox_Enter(object sender, EventArgs e)
        {
            if (Registration_textBox.Text == "Registration")
            {
                Registration_textBox.Text = "";
                Registration_textBox.ForeColor = Color.Black;
            }
        }

        

        private void tableLayoutPanel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            if(!Check_regNo())
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Student values (@FirstName,@LastName,@Contact,@Email,@RegistrationNumber,@Status)", con);
                cmd.Parameters.AddWithValue("@FirstName", textBox1.Text);
                cmd.Parameters.AddWithValue("@LastName", LastName_textBox.Text);
                cmd.Parameters.AddWithValue("@Contact", Contact_textBox.Text);
                cmd.Parameters.AddWithValue("@Email", Email_textBox.Text);
                cmd.Parameters.AddWithValue("@RegistrationNumber", Registration_textBox.Text);
                if(cbStatus.SelectedIndex == 0 )
                {
                    cmd.Parameters.AddWithValue("@Status", 5);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@Status", 6);
                }
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
                binddata();
                changeColorTextbox();
            }
            else
            {
                
                MessageBox.Show("registration number already exists");
            }
        }
        public void binddata()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private void changeColorTextbox()
        {
            textBox1.Text = "FirstName";
            textBox1.ForeColor = Color.Silver;
            Email_textBox.Text = "LastName";
            Email_textBox.ForeColor = Color.Silver;
            Registration_textBox.Text = "RegistrationNumber";
            Registration_textBox.ForeColor = Color.Silver;
            Contact_textBox.Text = "Contact";
            Contact_textBox.ForeColor = Color.Silver;
            LastName_textBox.Text = "LastName";
            LastName_textBox.ForeColor = Color.Silver;
        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns["Delete"].Index == e.ColumnIndex)
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("DELETE from student WHERE RegistrationNumber=@RegistrationNumber", con);
                cmd.Parameters.AddWithValue("@RegistrationNumber", dataGridView1.CurrentCell.OwningRow.Cells[7].Value.ToString());
                cmd.ExecuteNonQuery();
                changeColorTextbox();
                binddata();
            }
            if (dataGridView1.Columns["Update"].Index == e.ColumnIndex)
            {
                string id, fName, lName, contact, email, regNo, status;
                id = dataGridView1.CurrentCell.OwningRow.Cells[2].Value.ToString();
                fName = dataGridView1.CurrentCell.OwningRow.Cells[3].Value.ToString();
                lName =  dataGridView1.CurrentCell.OwningRow.Cells[4].Value.ToString();
                contact = dataGridView1.CurrentCell.OwningRow.Cells[5].Value.ToString();
                email = dataGridView1.CurrentCell.OwningRow.Cells[6].Value.ToString();
                regNo = dataGridView1.CurrentCell.OwningRow.Cells[7].Value.ToString();
                status = dataGridView1.CurrentCell.OwningRow.Cells[8].Value.ToString();
                Form f = new Student_Update_form(id,fName,lName,contact,email,regNo,status);
                f.ShowDialog();
                binddata();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (char.IsDigit(ch))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void LastName_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (char.IsDigit(ch))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void Contact_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (char.IsLetter(ch))
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }
        private bool Check_regNo()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Student where RegistrationNumber=@RegistrationNumber", con);
            cmd.Parameters.AddWithValue("@RegistrationNumber", Registration_textBox.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt.Rows.Count > 0;
        }
    }
}
